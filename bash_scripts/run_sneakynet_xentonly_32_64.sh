win_size=64
epochs=100000
save_every=25000
start_epoch=save_every
dataset_to_infer="infer"
gpu_id=7

model_path_stub=/data/"${USER}"/PhD/Experiments/UpperBodiesAll/20220310_sneakynet_xentonly_32_64
gt_dir=/home/"${USER}"/PhD/Data/WholeSkeletonsCleaned/Processed/upperBodiesAll

# Build docker image
cd /home/"${USER}"/PhD/Repos/sneakynet/
docker build -t tf2image --rm --network=host .

for ((fold=1; fold<=5; fold=fold+1)); do
  model_path="${model_path_stub}"/fold_"${fold}"

  # Training
  docker run --shm-size 8G -t --init --rm --gpus '"device='${gpu_id}'"'  -v /home/"${USER}"/PhD/Repos:/home/"${USER}"/PhD/Repos  \
  -v /home/"${USER}"/PhD/Data:/home/"${USER}"/PhD/Data -v /data/"${USER}"/PhD/Experiments:/data/"${USER}"/PhD/Experiments \
  --name evatf2container4 tf2image:latest \
  python3 /home/"${USER}"/PhD/Repos/sneakynet/train_sneakynet_xentonly.py \
  --model_path="${model_path}" \
  --train_config=nonewnet/default_config.py \
  --train_config.padding=21 \
  --train_config.win_size="${win_size}" \
  --train_config.epochs="${epochs}" \
  --train_config.save_every="${save_every}" \
  --train_config.dataset_split_file=/home/"${USER}"/PhD/Repos/sneakynet/data/data_split_"${fold}".csv \


  for ((epoch=start_epoch; epoch<="${epochs}"; epoch=epoch+save_every)); do

    # Inference
    docker run --shm-size 8G -t --init --rm --gpus '"device='${gpu_id}'"'  \
      -v /home/"${USER}"/PhD/Repos:/home/"${USER}"/PhD/Repos  \
      -v /home/"${USER}"/PhD/Data:/home/"${USER}"/PhD/Data -v /data/"${USER}"/PhD/Experiments:/data/"${USER}"/PhD/Experiments \
      --name evatf2container4 tf2image:latest \
      python3 /home/"${USER}"/PhD/Repos/sneakynet/infer.py \
      --model_path="${model_path}" \
      --epoch="${epoch}" \
      --predictions_save_dir="${model_path}"/segmentation_output_"${dataset_to_infer}"/"${epoch}" \
      --dataset_to_infer="${dataset_to_infer}" \
      --infer_config=nonewnet/default_config.py \
      --infer_config.overlap=21 \
      --infer_config.padding=21 \
      --infer_config.win_size="${win_size}" \
      --infer_config.dataset_split_file=/home/"${USER}"/PhD/Repos/sneakynet/data/data_split_"${fold}".csv \

    # Evaluation

    pred_file_pattern=""${model_path}"/segmentation_output_"${dataset_to_infer}"/"${epoch}"/**.nii.gz"
    pred_file_list=("$pred_file_pattern")
    pred_file_array=($pred_file_list)


    csv_dir="${model_path}"/eval_"${dataset_to_infer}"_at_"${epoch}"

    for pred_file in "${pred_file_array[@]}"
    do
      subject_name=$(echo "${pred_file}" | grep -o '[0-9]\{3\}_upper')  # find something like 012_upper and save in variable
      gt_file="${gt_dir}"/"${subject_name}"/bones.nii.gz
      echo $file_name_gt
      docker run --shm-size 8G -t --init --rm --gpus=0  \
       -v /home/"${USER}"/PhD/Repos:/home/"${USER}"/PhD/Repos  \
       -v /home/"${USER}"/PhD/Data:/home/"${USER}"/PhD/Data -v /data/"${USER}"/PhD/Experiments:/data/"${USER}"/PhD/Experiments \
       --name evatf2container4 tf2image:latest \
       python3 /home/"${USER}"/PhD/Repos/sneakynet/eval.py --prediction="${pred_file}" --gt="${gt_file}" --save_csv="${csv_dir}"
    done

  done

done
