import logging
import os
import random
from datetime import datetime

from absl import app
from absl import flags
from ml_collections.config_flags import config_flags
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow.python.framework.ops import enable_eager_execution

from nonewnet.data_io import add_volume_padding, GeneratorAdapter, \
    get_balanced_train_sampler, get_image_readers_with_prior
from global_config import MODEL_DIR_NAME
from nonewnet.loss import DicePlusXentLoss
from nonewnet.models.model_baseline_slim import UNet3D
from nonewnet.types import Action


EAGER = True

if EAGER:
    enable_eager_execution(
        config=None,
        device_policy=None,
        execution_mode=None
    )
seed_value = 0
random.seed(seed_value)
np.random.seed(seed_value)
tf.random.set_seed(seed_value)

print('Start Training: \n')
print('Tensor Flow version: ', tf.version.VERSION, '\n')
print('eager execution: ', tf.executing_eagerly()
      )
FLAGS = flags.FLAGS
# config_flags.DEFINE_config_file('train_config', 'nonewnet/default_config.py')
config_flags.DEFINE_config_file('train_config', 'nonewnet/mock_data_config.py')
flags.DEFINE_string("model_path", '/path_to_file//Repos/sneakynet/debugmodel', "Path to save the keras model.")


class WriteLossToLoggerCallback(tf.keras.callbacks.Callback):

    def __init__(self, tensorboard_log_dir):
        self.time_previous = datetime.now()
        self.tensorboard_summary_writer = tf.summary.create_file_writer(logdir=tensorboard_log_dir)

    def on_train_begin(self, logs=None):
        logging.info(f'Start Training:')
        self.time_previous = datetime.now()

    def on_epoch_end(self, epoch, logs=None):
        metrics_to_print = ''
        total_loss = logs['loss']
        primary_loss = float(total_loss)

        for log_key in logs.keys():
            if 'metric' in log_key:
                metrics_to_print = metrics_to_print + f', {log_key}: {logs[log_key]:.5}'
                primary_loss = primary_loss - float(logs[log_key])

                with self.tensorboard_summary_writer.as_default():
                    tf.summary.scalar(log_key, logs[log_key], step=epoch)

        later = datetime.now()
        difference = (later - self.time_previous).total_seconds()
        logging.info(
            f'Epoch {epoch:5}, Total Loss: {total_loss:.5}, Primary Loss: {primary_loss:.5}, {metrics_to_print}, ({difference:.4}s)')
        with self.tensorboard_summary_writer.as_default():
            tf.summary.scalar('Primary Loss', primary_loss, step=epoch)

        self.time_previous = datetime.now()

    def on_train_end(self, logs=None):
        logging.info(f'Finished Training.')


class SaveModelCallback(tf.keras.callbacks.Callback):

    def __init__(self, save_freq, filepath):
        super(SaveModelCallback, self).__init__()
        self.save_freq = save_freq
        self.filepath = filepath

    def on_epoch_end(self, epoch, logs=None):
        if epoch != 0 and epoch % self.save_freq == 0:
            tf.keras.models.save_model(
                self.model, os.path.join(self.filepath, str(epoch)), overwrite=True, include_optimizer=True,
                save_format="tf"
            )


def train(config, model_path):
    # Get dataset

    # Do we use a prior as input as well? Binary prior goes first
    prior_csv=[]
    first_prior_binary = False
    if config.binary_csv:
        prior_csv.append(config.binary_csv)
        first_prior_binary = True
    if len(config.prior_csv) > 0:
        prior_csv.extend(config.prior_csv)
    num_priors = len(prior_csv)

    num_input_channels = 1 + num_priors  # the usual input plus the priors

    readers = get_image_readers_with_prior(action=Action.train, label_csv=config.label_csv, ct_csv=config.ct_csv,
                                           dataset_split_file=config.dataset_split_file, win_size=config.win_size,
                                           prior_csv_list=prior_csv, dataset_to_infer=None)

    readers = add_volume_padding(readers, padding=config.padding)

    sampler = get_balanced_train_sampler(readers, config.win_size, batch_size=config.batch_size)

    generator = GeneratorAdapter(sampler, num_priors)
    data_generator = generator.train_generator_with_prior

    dset = tf.data.Dataset.from_generator(generator=data_generator,
                                          output_signature=(
                                              tf.TensorSpec(
                                                  shape=(num_input_channels,
                                                         config.batch_size, config.win_size, config.win_size,
                                                         config.win_size,
                                                         1),
                                                  dtype=tf.float32),
                                              (tf.TensorSpec(
                                                  shape=(
                                                      config.batch_size, config.win_size, config.win_size,
                                                      config.win_size,
                                                      1),
                                                  dtype=tf.float32))
                                          )
                                          )

    # Plot some input images

    NUM_SAMPLES = 6  # how many subjects are drawn to visualise
    NUM_MODES = 2
    if num_priors > 0:
        NUM_MODES = NUM_MODES + num_priors

    fig, axs = plt.subplots(NUM_SAMPLES, NUM_MODES, figsize=(5, int(NUM_SAMPLES * 5 / NUM_MODES)))
    for idx, dataset_entry in enumerate(dset.take(NUM_SAMPLES)):
        volume = dataset_entry[0].numpy()[0]
        axs[idx, 0].imshow(volume[0, :, int(config.win_size / 2), :, 0])

        label = dataset_entry[1].numpy()
        axs[idx, 1].imshow(label[0, :, int(config.win_size / 2), :, 0])

        if num_priors > 0:
            prior = dataset_entry[0].numpy()[1]
            axs[idx, 2].imshow(prior[0, :, int(config.win_size / 2), :, 0])
            if num_priors > 1:
                prior = dataset_entry[0].numpy()[2]
                axs[idx, 3].imshow(prior[0, :, int(config.win_size / 2), :, 0])
                if num_priors > 2:
                    prior = dataset_entry[0].numpy()[3]
                    axs[idx, 4].imshow(prior[0, :, int(config.win_size / 2), :, 0])
                    if num_priors > 3:
                        prior = dataset_entry[0].numpy()[4]
                        axs[idx, 5].imshow(prior[0, :, int(config.win_size / 2), :, 0])

    fig.show()
    input_images_save_dir = os.path.join(model_path, 'input_images')
    if not os.path.exists(input_images_save_dir):
        os.makedirs(input_images_save_dir)
    fig.savefig(os.path.join(input_images_save_dir, 'train_input_samples.png'))

    keras_model = UNet3D(num_classes=config.num_classes, num_priors=num_priors,
                         with_loc_loss_gradient=config.with_loc_loss_gradient,
                         output_binary_segmentation=config.output_binary_segmentation,
                         concat_before_end_convs=config.concat_before_end_convs, concat_start=config.concat_start,
                         concat_before_classification=config.concat_before_classification,
                         output_loc_prior=config.output_loc_prior, first_prior_binary=first_prior_binary)

    optimizer = tf.keras.optimizers.Adam(
        learning_rate=0.001,
        beta_1=0.9,
        beta_2=0.999,
        epsilon=1e-08)
    # keras_model.compile(optimizer=optimizer, loss=DicePlusXentLoss(), metrics=None, loss_weights=None,
    #                     weighted_metrics=None, run_eagerly=EAGER)
    keras_model.compile(optimizer=optimizer, loss=DicePlusXentLoss(), metrics=None, loss_weights=None,
                        weighted_metrics=None, run_eagerly=EAGER)
    # keras_model.compile(optimizer=optimizer, loss=HierarchicalLoss(), metrics=None, loss_weights=None,
    #                     weighted_metrics=None, run_eagerly=EAGER)

    save_model_path = os.path.join(model_path, MODEL_DIR_NAME)

    tensorboard_log_dir = os.path.join(model_path, 'tensorboard_logs')
    my_callbacks = [
        WriteLossToLoggerCallback(tensorboard_log_dir=tensorboard_log_dir),
        SaveModelCallback(save_freq=config.save_every, filepath=save_model_path),
        tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_dir),
        # WriteImagesToTensorboardCallback(valid_data=dataset_entry, log_dir=tensorboard_log_dir,
        #                                  output_location_predictions=config.output_loc_prior,
        #                                  output_binary_predictions=config.output_binary_segmentation)
    ]
    keras_model.build(input_shape=[(config.batch_size, config.win_size, config.win_size, config.win_size, 1),
                                   (config.batch_size, config.win_size, config.win_size, config.win_size, 1),
                                   (config.batch_size, config.win_size, config.win_size, config.win_size, 1),
                                   (config.batch_size, config.win_size, config.win_size, config.win_size, 1),
                                   (config.batch_size, config.win_size, config.win_size, config.win_size, 1)])
    keras_model.summary(print_fn=lambda x: logging.info(x))

    keras_model.fit(dset, epochs=config.epochs,
                    steps_per_epoch=1,
                    callbacks=my_callbacks
                    )

    tf.keras.models.save_model(
        keras_model, os.path.join(save_model_path, str(config.epochs)), overwrite=True, include_optimizer=True,
        save_format="tf"
    )


def run_train(_):
    if not os.path.exists(FLAGS.model_path):
        os.makedirs(FLAGS.model_path)

    # Need to remove the logging handlers, otherwise it will not write to file.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                        filename=os.path.join(FLAGS.model_path, 'train_log.log'), filemode='w+', level=logging.INFO,
                        datefmt='%m-%d %H:%M')

    with open(os.path.join(FLAGS.model_path, 'train_config.txt'), 'w+') as f:
        f.write(f'Training using the following configs and args: \n'
                f'{FLAGS.train_config} \n'
                f'--model_path {FLAGS.model_path} \n')
    train(config=FLAGS.train_config, model_path=FLAGS.model_path)


if __name__ == '__main__':
    app.run(run_train)
