# Run using
# docker run --shm-size 8G -t --init --rm --gpus=0  \
# -v /home/eva/PhD:/home/eva/PhD  --name evatf2container tf2image:latest \
# python3 /home/eva/PhD/Repos/sneakynet/infer_specto.py --infer_config.overlap=24
import csv
import glob
import json
import logging
import os
import random
import sys
from typing import Union, List

# os.environ["CUDA_VISIBLE_DEVICES"]="-1" # uncomment for CPU only computations

from absl import app, flags
import ml_collections
from ml_collections.config_flags import config_flags
import nibabel
import numpy as np
import skimage.transform as skTrans
import tensorflow as tf
from tensorflow.python.framework.ops import enable_eager_execution

import nonewnet.loss
import nonewnet.default_config
from nonewnet.data_io import add_volume_padding, GeneratorAdapter, \
    get_grid_inference_sampler, get_image_readers_with_prior
from global_config import NIFTY_NET_PATH, MODEL_DIR_NAME
from nonewnet.types import Action, DatasetToInfer
from nonewnet import utils

FILENAME_INDEX_JSON = 'index.json'  # Do NOT change this, except if you change it inside Specto too!
FILENAME_OUTPUT_SEGMENTATION = 'segmentation.bin'

sys.path.insert(0, NIFTY_NET_PATH)
from niftynet.io.image_reader import ImageReader
from niftynet.engine.windows_aggregator_grid import GridSamplesAggregator
from niftynet.io.image_sets_partitioner import ImageSetsPartitioner
from niftynet.layer.pad import PadLayer
from niftynet.utilities.util_common import ParserNamespace
from niftynet.engine.sampler_grid_v2 import GridSampler, grid_spatial_coordinates

EAGER = True

if EAGER:
    enable_eager_execution(
        config=None,
        device_policy=None,
        execution_mode=None
    )

seed_value = 0
random.seed(seed_value)
np.random.seed(seed_value)
tf.random.set_seed(seed_value)

print('Start Inference: \n')
print('Tensor Flow version: ', tf.version.VERSION, '\n')
print('eager execution: ', tf.executing_eagerly()
      )
FLAGS = flags.FLAGS

config_flags.DEFINE_config_file('infer_config', 'nonewnet/specto_config.py')
flags.DEFINE_string("model_path",
                    '/home/eva/PhD/Repos/sneakynet/debugmodel/models/10',
                    "This directory should contain the subdirectiories assets, and variables, as well as the files keras_metadata.pb and saved_model.pb")
flags.DEFINE_string("predictions_save_dir", 'debug_images/prediction',
                    "Path where the output segmentation and index.json will be saved.")
flags.DEFINE_string("input_file_path",
                    '/home/eva/PhD/SpectoXR-Segmenter-StandaloneLinux64/SpectoDataProcessor/demo/workdir/244254de70004590b8f991cd5c2d7692/input/dataset.nii',
                    "Path where the input file comes from.")
flags.DEFINE_string("regions_colormap_file_path",
                    '/home/eva/PhD/SpectoXR-Segmenter-StandaloneLinux64/SpectoDataProcessor/demo/workdir/244254de70004590b8f991cd5c2d7692/input/dataset.nii',
                    "Path where the input file comes from.")


def infer(config: ml_collections.ConfigDict(),
          model_path: Union[str, os.PathLike],
          predictions_save_dir: Union[str, os.PathLike],
          input_file_path: Union[str, os.PathLike],
          regions_colormap_file_path: Union[str, os.PathLike]):
    """ Load scan, preprocess, segment, postprocess, save resulting segmentation.

    Args:
        config: Inference parameters.
        model_path: Path to a trained model which will be used for inference.
        predictions_save_dir: Path to a dir where the final segmentation and index.json files are saved to.
        input_file_path: Path to the input nifty file.
        regions_colormap_file_path: Path to the regions file that will be used to generate index.json

    Returns:

    """
    intermediate_results_dir = os.path.join(predictions_save_dir, 'intermediate_files')
    if not os.path.exists(intermediate_results_dir):
        os.makedirs(intermediate_results_dir)
    output_file_path_downsampled = os.path.join(intermediate_results_dir, 'downscaled.nii.gz')
    output_file_path_index_json = os.path.join(predictions_save_dir, FILENAME_INDEX_JSON)

    print('status{Preprocessing.}')
    preprocessing(input_file_path, output_file_path_downsampled, output_file_path_index_json,
                  regions_colormap_file_path)

    # Create the ct, label and data split files which the we need for the image reader
    scan_csv_file_path = os.path.join(intermediate_results_dir, 'ct.csv')
    with open(scan_csv_file_path, 'w+', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|')
        writer.writerow(['image_0', output_file_path_downsampled])

    dataset_split_file_path = os.path.join(intermediate_results_dir, 'data_split.csv')
    with open(dataset_split_file_path, 'w+', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|')
        writer.writerow(['image_0', 'inference'])

    num_priors = len(config.prior_csv)
    num_input_channels = 1 + num_priors  # the usual input plus the priors

    readers = get_image_readers_with_prior(action=Action.infer, label_csv=config.label_csv, ct_csv=scan_csv_file_path,
                                           dataset_split_file=dataset_split_file_path, win_size=config.win_size,
                                           prior_csv_list=config.prior_csv, dataset_to_infer=DatasetToInfer.infer)

    readers = add_volume_padding(readers, padding=config.padding)

    sampler = get_grid_inference_sampler(readers, window_size=config.win_size, infer_overlap=config.overlap)

    generator = GeneratorAdapter(sampler, num_priors)
    data_generator = generator.inference_generator_with_prior

    dset = tf.data.Dataset.from_generator(generator=data_generator,
                                          output_signature=(
                                              tf.TensorSpec(shape=(num_input_channels,  # the model input
                                                                   config.batch_size, config.win_size, config.win_size,
                                                                   config.win_size, 1),
                                                            dtype=tf.float32),
                                              tf.TensorSpec(shape=(1, 7),
                                                            # the image_location, needed to reassemble the whole scan from individual patches.
                                                            dtype=tf.float32)
                                          )
                                          )

    # Load model
    print('status{Load model.}')
    custom_objects = {"DicePlusXentLoss": nonewnet.loss.DicePlusXentLoss}
    keras_model = tf.keras.models.load_model(
        model_path, custom_objects=custom_objects, compile=True, options=None
    )

    # Create an output aggregator that will stitch the inferred patches together
    output_aggregator = GridSamplesAggregator(image_reader=sampler.reader,
                                              window_border=(config.overlap, config.overlap, config.overlap),
                                              output_path=intermediate_results_dir,
                                              interp_order=0,
                                              fill_constant=0.0)

    coords = grid_spatial_coordinates(
        subject_id=1,
        img_sizes=readers[0].shapes,
        win_sizes={'image': (config.win_size, config.win_size, config.win_size, 1, 1)},
        border_size=(config.overlap, config.overlap, config.overlap))
    n_locations = list(coords.values())[0].shape[0]

    print('status{{Prepare inference of {} windows.}}'.format(n_locations))

    counter = 1
    with tf.keras.utils.custom_object_scope(custom_objects):
        # Inference for one patch (and hence also batch) at a time
        logging.info('Start inference:')
        for data_batch in dset:
            image = data_batch[0]
            image_location = data_batch[1].numpy()
            # Inference
            net_output_tensor = keras_model(image, training=False)
            # Postprocessing
            net_output_tensor = tf.cast(tf.argmax(net_output_tensor, -1), tf.int32)
            net_output_tensor = tf.expand_dims(net_output_tensor, axis=-1)
            logging.info(f'Inference of a patch with output_tensor shape {net_output_tensor.shape}')
            print('status{{Inference of patch {:>5}/{}.}}'.format(str(counter), str(n_locations)))
            progress = 0.25 + 0.6 * counter / n_locations  # we display progress of model between 25% and 85% for the time being
            print('progress{{{}}}'.format(str(progress)))
            outputs = net_output_tensor.cpu().numpy()
            # Add inferred patch to the aggregator
            output_aggregator.decode_batch({'window_seg': outputs}, image_location)
            counter = counter + 1

    logging.info('Finished inference.')

    print('status{Postprocessing}')

    output_file_path = os.path.join(predictions_save_dir, FILENAME_OUTPUT_SEGMENTATION)
    postprocessing(intermediate_results_dir, output_file_path)


def postprocessing(predictions_save_dir: Union[str, os.PathLike], output_file_path):
    """ Reshape segmentation to 1D and save as raw bytes file.

    Args:
        predictions_save_dir: Directory where the segmentation has been saved to.
        output_file_path: File path were the final segmentation in specto format will be saved to.

    Returns:

    """
    # Save as .bin file
    nifty_path = glob.glob('{}/*_seg_*.nii.gz'.format(predictions_save_dir))[0]
    nifty_file: nibabel.Nifti1Image = nibabel.load(nifty_path)
    nifty_file_array = nifty_file.get_fdata()
    print('output shape: {}'.format(nifty_file_array.shape))
    reshaped_array = nifty_file_array.astype(np.ubyte).flatten(
        'F')  # flatten 3d array to 1d and change type to unsigned 8bit integer.
    print('progress{{{}}}'.format(str(0.95)))
    bytes_array = reshaped_array.tofile(output_file_path, sep='',
                                        format='%s')  # write to bytes file


def preprocessing(input_file_path: Union[str, os.PathLike],
                  output_file_path_downsampled: Union[str, os.PathLike],
                  output_file_path_index_json: Union[str, os.PathLike],
                  colormap_file_path: Union[str, os.PathLike]):
    """ Downsample scan to 2mm resolution and create matching index.json.

    Args:
        input_file_path: Path to the input nifty file.
        output_file_path_downsampled: Where to save the downsampled file.
        output_file_path_index_json: Where to save index json file.
        colormap_file_path: Path to the file that lists all regions and is used to create index.json

    Returns:

    """
    print('status{Load scan.}')
    original_file: nibabel.Nifti1Image = nibabel.load(input_file_path)

    original_shape = original_file.shape
    original_spacings = original_file.header.get_zooms()
    new_spacing = 2.0
    new_shape = [int(original_shape[i] * original_spacings[i] / new_spacing) for i in (0, 1, 2)]
    print('status{Write index.json}')
    create_index_json(colormap_file_path, new_shape, output_file_path_index_json)
    print('status{Rescale scan}')
    print('new shape: {}'.format(new_shape))
    resized_data = skTrans.resize(original_file.get_fdata(), new_shape, order=1, preserve_range=True)
    print('status{Clamp values.}')
    # specto preprocessing means that many values were artificially put to -32k, but our algo is not trained on that.
    resized_data[resized_data < -1024] = -1024
    # we changed the resolution, so we need to compute a new affine for the nifty file.
    new_affine = utils.rescale_affine(affine=original_file.affine, shape=original_shape, zooms=original_spacings,
                                      new_shape=new_shape)
    # save the downsampled nifty file
    new_file = nibabel.Nifti1Image(resized_data, new_affine)

    nibabel.save(new_file, output_file_path_downsampled)


def create_index_json(colormap_file_path: Union[str, os.PathLike],
                      output_shape: List[int],
                      output_file_path: Union[str, os.PathLike]):
    """ Create the index.json file file that describes the segmentation output.

    Args:
        colormap_file_path: A 3d slicer like colormap file containing name, label and color of each class.
        output_shape: The 3d shape of the segmentation output that will be delivered to specto [x,y,z].
        output_file_path: A file path where the resulting index.json will be saved to.

    """
    label_list = []

    if colormap_file_path.endswith('ctbl'):
        delimiter = ' '
    elif colormap_file_path.endswith('csv'):
        delimiter = ','
    colormap_data = np.genfromtxt(colormap_file_path, delimiter=delimiter, dtype=str)
    colormap_columns = colormap_data.transpose()

    labels = colormap_columns[0]
    names = colormap_columns[1]
    rs = colormap_columns[2]
    gs = colormap_columns[3]
    bs = colormap_columns[4]

    for idx in range(len(labels)):
        label = int(labels[idx])
        r = int(rs[idx]) / 255.0
        g = int(gs[idx]) / 255.0
        b = int(bs[idx]) / 255.0

        label_dict = {'color': [r, g, b, 1.0], 'value': label, 'description': names[idx]}
        if label != 0:
            label_list.append(label_dict)

    file_dict = {'mask':
                     {'file': FILENAME_OUTPUT_SEGMENTATION,
                      'format': 'r8ui',
                      'resolution': output_shape,
                      'labels': label_list}
                 }

    with open(output_file_path, 'w') as f:
        json.dump(file_dict, f)


def run_infer(_):
    if not os.path.exists(FLAGS.predictions_save_dir):
        os.makedirs(FLAGS.predictions_save_dir)

    with open(os.path.join(FLAGS.predictions_save_dir, 'inference_config.txt'), 'w+') as f:
        f.write(f'Inference using the following configs and args: \n'
                f'{FLAGS.infer_config} \n'
                f'--model_path {FLAGS.model_path} \n'
                f'--predictions_save_dir: {FLAGS.predictions_save_dir} \n'
                f'--input_file_path: {FLAGS.input_file_path} \n'
                f'--regions_colormap_file_path: {FLAGS.regions_colormap_file_path} \n')

    infer(config=FLAGS.infer_config, model_path=FLAGS.model_path, predictions_save_dir=FLAGS.predictions_save_dir,
          input_file_path=FLAGS.input_file_path, regions_colormap_file_path=FLAGS.regions_colormap_file_path)
    for name in list(flags.FLAGS):
        delattr(flags.FLAGS, name)


if __name__ == '__main__':
    flags.register_validator('model_path', lambda path: os.path.isdir(path),
                             message='--model_path must be a path to an existing directory')
    flags.register_validator('regions_colormap_file_path', lambda path: os.path.isfile(path),
                             message='--regions_colormap_file_path must be a path to an existing file.')

    app.run(run_infer)
