# Run e.g. with
# docker run --shm-size 8G -t --init --rm --gpus=0  -v /path_to_file/:/path_to_file/  \
#  --name evatf2container tf2image:latest \
# python3 /path_to_file//Repos/sneakynet/eval.py \
# --gt=/path_to_file//Data/WholeSkeletonsCleaned/Processed/upperBodiesAll/002_upper/bones.nii.gz \
# --prediction=debug_images/prediction/window_image_002_upper_niftynet_out.nii.gz\
# --save_csv=debug_images/prediction

from dataclasses import dataclass
import logging
import os
import sys

from absl import app
from absl import flags
import nibabel as nib
import numpy as np

from global_config import NIFTY_NET_PATH

sys.path.insert(0, NIFTY_NET_PATH)
from niftynet.evaluation.base_evaluator import CachedSubanalysisEvaluator
from niftynet.layer.base_layer import Layer

FLAGS = flags.FLAGS
flags.DEFINE_string("prediction", None, "File path of the prediction.")
flags.DEFINE_string("gt", None, "File path of the ground truth.")
flags.DEFINE_string("save_csv", None, "Directory where to save the results csv.")
flags.DEFINE_integer("num_classes", 126, "The number of classes present in the segmentations.")


class ReaderGenerator(Layer):
    subjects = []

    def __init__(self, file_paths):
        """
        :param file_paths: e.g [{'prediction': '/file/1/scan.nii.gz', 'label': '/file/1/gt.nii.gz}]
        """
        super(ReaderGenerator, self).__init__()
        self.subjects = file_paths
        self.current_id = -1
        self.subject_ids = {}
        for int_id, subject in enumerate(self.subjects):
            self.subject_ids[int_id] =os.path.basename(subject['prediction'])

    def layer_op(self, shuffle=False):
        idx = self.current_id + 1
        self.current_id = idx

        try:
            subject = self.subjects[idx]
        except (IndexError, TypeError):
            return -1, None, None

        prediction_nifty = nib.load(subject['prediction'])
        ground_truth_nifty = nib.load(subject['label'])

        prediction_data = prediction_nifty.get_fdata().astype(dtype=np.uint8)
        ground_truth_data = ground_truth_nifty.get_fdata().astype(dtype=np.uint8)

        image_data_dict = {'inferred': prediction_data, 'label': ground_truth_data}
        interp_order_dict = {'inferred': 0, 'label': 0}
        return idx, image_data_dict, interp_order_dict

    def get_subject_id(self, image_index):
        """
        Given an integer id returns the subject id.
        """
        try:
            return self.subject_ids[image_index]
        except KeyError:
            tf.compat.v1.logging.warning('Unknown subject id in reader file list.')
            raise


@dataclass
class EvalParams:
    """Could be used to override the default_evaluation_list, e.g. evaluations='n_pos_ref,dice'."""
    evaluations = None


@dataclass
class AppParams:
    output_prob: bool = False
    num_classes: int = 126
    evaluation_units: str = 'label'  # [foreground|label]


class GeneratorEvaluator(CachedSubanalysisEvaluator):

    def __init__(self, reader, app_param, eval_param):
        super(GeneratorEvaluator, self).__init__(reader, app_param, eval_param)

    def default_evaluation_list(self):
        """
        :return:  list of metric names to compute by default
        """
        return ['n_pos_ref', 'n_neg_ref', 'dice', 'tp', 'fp', 'fn', 'tn']


def evaluate_scans(file_paths, save_csv_dir, num_classes):
    """
    Evaluate dice scores of the given files
    :param file_paths: e.g [{'prediction': '/file/1/scan.nii.gz', 'label': '/file/1/gt.nii.gz}]
    :param save_csv_dir: Location where the results will be saved.
    :param num_classes: Number of labels present in the segmentations.
    :return:
    """
    generator_reader = ReaderGenerator(file_paths)
    evaluator = GeneratorEvaluator(reader=generator_reader,
                                   app_param=AppParams(num_classes=num_classes),
                                   eval_param=EvalParams())
    all_results = evaluator.evaluate()
    for group_by, data_frame in all_results.items():
        if group_by == (None,):
            csv_id = ''
        else:
            csv_id = '_'.join(group_by)
        if not os.path.isdir(save_csv_dir):
            os.makedirs(save_csv_dir)
        with open(os.path.join(save_csv_dir,
                               'eval_' + csv_id + '.csv'), 'a') as csv:
            csv.write(data_frame.reset_index().to_csv(index=False))


def run_evaluation(_):
    if not os.path.exists(FLAGS.save_csv):
        os.makedirs(FLAGS.save_csv)
    file_paths = [{'prediction': FLAGS.prediction, 'label': FLAGS.gt}]

    # Need to remove the logging handlers, otherwise it will not write to file.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                        filename=os.path.join(FLAGS.save_csv, 'evaluation_log.log'),
                        filemode='w+', level=logging.INFO, datefmt='%m-%d %H:%M')

    with open(os.path.join(FLAGS.save_csv, 'eval_config.txt'), 'a+') as f:
        f.write(f'Inference using the following configs and args: \n'
                f'--prediction {FLAGS.prediction} \n'
                f'--gt {FLAGS.gt} \n'
                f'--save_csv: {FLAGS.save_csv} \n')
    logging.info('Start Evaluation.')

    evaluate_scans(file_paths, FLAGS.save_csv, num_classes=FLAGS.num_classes)


if __name__ == '__main__':
    flags.mark_flags_as_required(['prediction', 'gt', 'save_csv'])
    # Ensure all the files passed by command line exist.
    flags.register_validator('prediction', lambda path: os.path.isfile(path),
                             message='--prediction must be a path to an existing file')
    flags.register_validator('gt', lambda path: os.path.isfile(path),
                             message='--gt must be a path to an existing file')
    app.run(run_evaluation)
