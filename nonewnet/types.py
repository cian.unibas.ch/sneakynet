import enum
import sys

from global_config import NIFTY_NET_PATH

sys.path.insert(0, NIFTY_NET_PATH)
from niftynet.engine.signal import INFER, VALID


class Action(enum.Enum):
    train = 'TRAIN'
    infer = 'INFER'
    evaluate = 'EVALUATE'


class DatasetToInfer(enum.Enum):
    infer = INFER
    valid = VALID
