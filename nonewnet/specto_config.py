import ml_collections

def get_config():
    cfg = ml_collections.ConfigDict()
    cfg.win_size = 64
    cfg.padding = 24
    cfg.overlap = 5
    cfg.batch_size = 1
    cfg.prior_csv = ()
    cfg.with_loc_loss_gradient = False
    cfg.num_classes = 126
    cfg.label_csv = ""
    return cfg

