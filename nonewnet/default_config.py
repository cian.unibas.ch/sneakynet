import ml_collections

def get_config():
    cfg = ml_collections.ConfigDict()
    cfg.num_classes = 126  # segmentation classes including background
    cfg.epochs = 50000
    cfg.save_every = 5000
    cfg.win_size = 64
    cfg.padding = 20  # when loading the scan pad this much on every side
    cfg.overlap = 20  # when doing inference, patches overlap by this amount. If overlap > padding, the final image will be smaller than the output
    cfg.batch_size = 1
    cfg.ct_csv = '/path_to_file//Repos/sneakynet/data/ct_volumes.csv'
    cfg.label_csv = '/path_to_file//Repos/sneakynet/data/ground_truth_segmentations.csv'
    cfg.dataset_split_file = '/path_to_file//Repos/sneakynet/data/data_split_1.csv'
    cfg.prior_csv = ()  # legacy
    cfg.binary_csv = ''  # legacy
    cfg.output_binary_segmentation = False  # legacy
    cfg.output_loc_prior = False  # legacy
    cfg.with_loc_loss_gradient = False  # legacy
    cfg.concat_before_end_convs = False  # legacy
    cfg.concat_before_classification = False  # legacy
    cfg.concat_start = False  # legacy
    return cfg

