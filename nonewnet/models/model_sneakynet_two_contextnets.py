from __future__ import absolute_import, print_function
import sys

import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa

from global_config import NIFTY_NET_PATH
from nonewnet.loss import DicePlusXentLoss

sys.path.insert(0, NIFTY_NET_PATH)
from niftynet.layer import layer_util
from niftynet.layer.affine_augmentation import AffineAugmentationLayer


class DownSampleLayer(tf.keras.layers.Layer):
    def __init__(self,
                 func,
                 kernel_size=3,
                 stride=2,
                 padding='SAME',
                 name='pooling'):
        super(DownSampleLayer, self).__init__()

        self.func = func.upper()
        self.layer_name = '{}_{}'.format(self.func.lower(), name)
        self.padding = padding.upper()
        self.kernel_size = kernel_size
        self.stride = stride

    def call(self, input_tensor):
        kernel_size_all_dims = [self.kernel_size, self.kernel_size, self.kernel_size]
        stride_all_dims = [self.stride, self.stride, self.stride]

        output_tensor = tf.nn.pool(
            input=input_tensor,
            window_shape=kernel_size_all_dims,
            pooling_type=self.func,
            padding=self.padding,
            strides=stride_all_dims,
            name=self.layer_name)
        return output_tensor


class LinearResizeLayer(tf.keras.layers.Layer):
    """
    Resize 2D/3D images using ``tf.image.resize_bilinear``
    (without trainable parameters).
    """

    def __init__(self, new_size):
        """

        :param new_size: integer or a list of integers set the output
            2D/3D spatial shape.  If the parameter is an integer ``d``,
            it'll be expanded to ``(d, d)`` and ``(d, d, d)`` for 2D and
            3D inputs respectively.
        :param name: layer name string
        """
        super(LinearResizeLayer, self).__init__()
        self.new_size = new_size

    def build(self, input_shape):
        self.input_tensor_shape = input_shape

    def call(self, input_tensor):
        """
        Resize the image by linearly interpolating the input
        using TF ``resize_bilinear`` function.

        :param input_tensor: 2D/3D image tensor, with shape:
            ``batch, X, Y, [Z,] Channels``
        :return: interpolated volume
        """

        b_size, x_size, y_size, z_size, c_size = self.input_tensor_shape.as_list()

        x_size_new, y_size_new, z_size_new = self.new_size

        if (x_size == x_size_new) and (y_size == y_size_new) and (
                z_size == z_size_new):
            # already in the target shape
            return input_tensor

        # resize y-z

        squeeze_b_x = tf.reshape(
            input_tensor, tf.constant([-1, y_size, z_size, c_size]))
        resize_b_x = tf.compat.v1.image.resize_bilinear(
            squeeze_b_x, tf.constant([y_size_new, z_size_new]))
        resume_b_x = tf.reshape(
            resize_b_x, tf.constant([b_size, x_size, y_size_new, z_size_new, c_size]))

        # resize x
        #   first reorient
        reoriented = tf.transpose(resume_b_x, [0, 3, 2, 1, 4])
        #   squeeze and 2d resize
        squeeze_b_z = tf.reshape(
            reoriented, [-1, y_size_new, x_size, c_size])
        resize_b_z = tf.compat.v1.image.resize_bilinear(
            squeeze_b_z, [y_size_new, x_size_new])
        resume_b_z = tf.reshape(
            resize_b_z, [b_size, z_size_new, y_size_new, x_size_new, c_size])

        output_tensor = tf.transpose(resume_b_z, [0, 3, 2, 1, 4])
        return output_tensor


def nifty_net_default_w_initializer():
    def _initializer(shape, dtype, partition_info):
        stddev = np.sqrt(2.0 / np.prod(shape[:-1]))
        from tensorflow.python.ops import random_ops
        return random_ops.truncated_normal(shape, 0.0, stddev, dtype=tf.float32)

    return _initializer


def default_b_initializer():
    return tf.constant_initializer(0.0)


class ConvolutionalLayer(tf.keras.layers.Layer):
    """
    This class defines a composite layer with optional components::

        convolution -> feature_normalization (default batch norm) -> activation -> dropout

    The b_initializer and b_regularizer are applied to the ConvLayer
    The w_initializer and w_regularizer are applied to the ConvLayer,
    the feature normalization layer, and the activation layer (for 'prelu')
    """

    def __init__(self,
                 n_output_chns,
                 kernel_size=3,
                 stride=1,
                 padding='SAME',
                 feature_normalization='instance',
                 group_size=-1,
                 acti_func=None,
                 preactivation=False,
                 moving_decay=0.9,
                 eps=1e-5,
                 padding_constant=0,
                 name="conv"):
        """
        :param padding_constant: constant applied with CONSTANT padding
        """
        super(ConvolutionalLayer, self).__init__()

        self.acti_func = acti_func
        self.feature_normalization = feature_normalization
        self.group_size = group_size
        self.preactivation = preactivation
        self.layer_name = '{}'.format(name)

        self.layer_name += '_in'  # instance norm
        if self.acti_func is not None:
            self.layer_name += '_{}'.format(self.acti_func)

        # for ConvLayer
        self.n_output_chns = n_output_chns
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.padding_constant = padding_constant

        # for BNLayer
        self.moving_decay = moving_decay
        self.eps = eps

        SEED = 42
        he_normal_initializer = tf.keras.initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='normal',
                                                                      seed=SEED)

        self.full_stride = [self.stride, self.stride, self.stride]

        self.conv_layer = tf.keras.layers.Conv3D(filters=self.n_output_chns,
                                                 kernel_size=(3, 3, 3),
                                                 strides=self.full_stride,
                                                 padding=self.padding,
                                                 use_bias=False,
                                                 kernel_initializer=he_normal_initializer)

        self.in_layer = tfa.layers.InstanceNormalization(axis=-1,
                                                         center=True,
                                                         scale=True,
                                                         epsilon=1e-5,
                                                         gamma_initializer=tf.constant_initializer(1.0))

    def call(self, input_tensor, training=None):

        if self.acti_func is not None:
            acti_layer = ActiLayer(
                func=self.acti_func,
                name='acti_')

        def activation(output_tensor):
            output_tensor = self.in_layer(output_tensor)
            if self.acti_func is not None:
                output_tensor = acti_layer(output_tensor)
            return output_tensor

        output_tensor = activation(self.conv_layer(input_tensor))

        return output_tensor


def leaky_relu(x, name):
    half_alpha = 0.01
    return (0.5 + half_alpha) * x + (0.5 - half_alpha) * abs(x)


class ActiLayer(tf.keras.layers.Layer):
    """
    Apply an element-wise non-linear activation function.
    """

    def __init__(self, func, regularizer=None, name='activation'):
        self.func = func.lower()
        self.layer_name = '{}_{}'.format(self.func, name)
        super(ActiLayer, self).__init__()

    def call(self, input_tensor, training=None):
        output_tensor = leaky_relu(input_tensor, name='acti')
        return output_tensor


class UNetBlock(tf.keras.layers.Layer):
    def __init__(self,
                 func,
                 n_chns,
                 kernels,
                 w_initializer=None,
                 w_regularizer=None,
                 with_downsample_branch=False,
                 acti_func='leakyrelu',
                 name='UNet_block'):
        super(UNetBlock, self).__init__(name=name)
        self.func = func

        self.kernels = kernels
        self.n_chns = n_chns
        self.with_downsample_branch = with_downsample_branch
        self.acti_func = acti_func

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

        kernel_size = self.kernels[0]
        n_features = self.n_chns[0]
        acti_func = self.acti_func if kernel_size > 1 else None
        feature_normalization = 'instance' if acti_func is not None else None
        self.conv_layer_1 = ConvolutionalLayer(n_output_chns=n_features,
                                               kernel_size=kernel_size,
                                               acti_func=acti_func,
                                               name='{}'.format(n_features),
                                               feature_normalization=feature_normalization)

        if len(self.kernels) >= 2:
            kernel_size = self.kernels[1]
            n_features = self.n_chns[1]
            acti_func = self.acti_func if kernel_size > 1 else None
            feature_normalization = 'instance' if acti_func is not None else None
            self.conv_layer_2 = ConvolutionalLayer(n_output_chns=n_features,
                                                   kernel_size=kernel_size,
                                                   acti_func=acti_func,
                                                   name='{}'.format(n_features),
                                                   feature_normalization=feature_normalization)
        if len(self.kernels) == 3:
            kernel_size = self.kernels[2]
            n_features = self.n_chns[2]
            acti_func = self.acti_func if kernel_size > 1 else None  # a kernel size of 1 means classification layer, so we don't add normalization and activation.
            feature_normalization = 'instance' if acti_func is not None else None
            self.conv_layer_3 = ConvolutionalLayer(n_output_chns=n_features,
                                                   kernel_size=kernel_size,
                                                   acti_func=acti_func,
                                                   name='{}'.format(n_features),
                                                   feature_normalization=feature_normalization)

        self.downsample_layer = DownSampleLayer('MAX', kernel_size=2, stride=2)

    def build(self, input_shape):
        up_shape = [2 * int(input_shape[i]) for i in (1, 2, 3)]
        self.upsample_op = LinearResizeLayer(up_shape)

    def call(self, thru_tensor, training=None):

        thru_tensor = self.conv_layer_1(thru_tensor)
        if len(self.kernels) >= 2:
            thru_tensor = self.conv_layer_2(thru_tensor)
        if len(self.kernels) == 3:
            thru_tensor = self.conv_layer_3(thru_tensor)

        if self.with_downsample_branch:
            branch_output = thru_tensor
        else:
            branch_output = None

        if self.func == 'DOWNSAMPLE':
            thru_tensor = self.downsample_layer(thru_tensor)
        elif self.func == 'UPSAMPLE':
            thru_tensor = self.upsample_op(thru_tensor)

        elif self.func == 'NONE':
            pass  # do nothing

        return thru_tensor, branch_output


class ElementwiseLayer(tf.keras.layers.Layer):
    """
    This class takes care of the elementwise sum in a residual connection
    It matches the channel dims from two branch flows,
    by either padding or projection if necessary.
    """

    def __init__(self,
                 func,
                 initializer=None,
                 regularizer=None,
                 name='residual'):

        self.func = func
        self.layer_name = '{}_{}'.format(name, self.func.lower())
        super(ElementwiseLayer, self).__init__(name=self.layer_name)

    def call(self, param_flow, bypass_flow):
        if self.func == 'CONCAT':
            output_tensor = tf.concat([param_flow, bypass_flow], axis=-1)
        else:
            raise NotImplementedError()

        return output_tensor


class LocalizationLoss(tf.keras.layers.Layer):
    """ An auxiliary loss which tries to predict the values of the location prior.

    """

    def __init__(self, metric_name, with_loc_loss_gradient=False):
        super(LocalizationLoss, self).__init__()
        self.metric_name = metric_name
        self.loss_function = tf.keras.losses.MeanSquaredError()
        self.dense_layer = tf.keras.layers.Dense(1)
        self.with_loc_loss_gradient = with_loc_loss_gradient

    def call(self, decoder_output, true_prior):
        # maybe try also with the gradient in the future.

        if not self.with_loc_loss_gradient:
            inputs_no_grad = tf.stop_gradient(decoder_output)
            predicted_prior = self.dense_layer(inputs_no_grad)
        else:
            predicted_prior = self.dense_layer(decoder_output)
        self.add_loss(self.loss_function(true_prior, predicted_prior))
        self.add_metric(tf.square(true_prior - predicted_prior), name=self.metric_name, aggregation="mean")

        # this is a pass trough layer
        return predicted_prior

class BinaryClassificationLoss(tf.keras.layers.Layer):
    """ An auxiliary loss which tries to predict the values of the binary classification.

    """

    def __init__(self, metric_name, with_gradient=False):
        super(BinaryClassificationLoss, self).__init__()
        self.metric_name = metric_name
        self.loss_function = DicePlusXentLoss()
        self.with_gradient = with_gradient

        self.initializers = {'w': None, 'b': None}
        self.regularizers = {'w': None, 'b': None}
        self.binary_classification_layer = UNetBlock('NONE',
                                              (2,),
                                              (1,),
                                              with_downsample_branch=False,
                                              w_initializer=self.initializers['w'],
                                              w_regularizer=self.regularizers['w'],
                                              acti_func=None,
                                              name='Binary_Classification')

    def call(self, thru_tensor, y_true, training=None):
        # maybe try also with the gradient in the future.

        if not self.with_gradient:
            thru_tensor = tf.stop_gradient(thru_tensor)

        binary_classification_prediction, _ = self.binary_classification_layer(thru_tensor)
        if training:
            self.add_loss(self.loss_function(y_true, binary_classification_prediction))
            self.add_metric(self.loss_function(y_true, binary_classification_prediction), name=self.metric_name, aggregation="mean")

        # this is a pass trough layer
        return binary_classification_prediction

class CoarseClassificationLoss(tf.keras.layers.Layer):
    """ An auxiliary loss which tries to predict the values of the binary classification.

    """

    def __init__(self, metric_name):
        super(CoarseClassificationLoss, self).__init__()
        self.metric_name = metric_name
        self.loss_function = DicePlusXentLoss()

    def call(self, y_true, y_predict, training=None):

        self.add_loss(self.loss_function(y_true, y_predict))
        self.add_metric(self.loss_function(y_true, y_predict), name=self.metric_name, aggregation="mean")

        # this is a pass through layer
        return y_predict


class UNet3D(tf.keras.Model):
    """
    Implementation of No New-Net
      Isensee et al., "No New-Net", MICCAI BrainLesion Workshop 2018.

      The major changes between this and our standard 3d U-Net:
      * input size == output size: padded convs are used
      * leaky relu as non-linearity
      * reduced number of filters before upsampling
      * instance normalization (not batch)
      * fits 128x128x128 with batch size of 2 on one TitanX GPU for
      training
      * no learned upsampling: linear resizing.
    """

    def __init__(self, num_classes, spatial_size_incl_surrounding, w_initializer=None, w_regularizer=None,
                 b_initializer=None, b_regularizer=None, acti_func='leakyrelu', name='NoNewNet', num_priors=0,
                 with_loc_loss_gradient=False, output_binary_segmentation=False, concat_before_end_convs=False,
                 concat_start=False, concat_before_classification=False, output_loc_prior=False,
                 first_prior_binary=False):
        super(UNet3D, self).__init__(name=name)

        self.n_features = [30, 60, 120, 240, 480]
        self.acti_func = acti_func
        self.num_classes = num_classes
        print('num_classes', self.num_classes)

        self.initializers = {'w': w_initializer, 'b': b_initializer}
        self.regularizers = {'w': w_regularizer, 'b': b_regularizer}

        print('using {}'.format(name))

        self.down_layer_1 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[0], self.n_features[0]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='L1')
        self.down_layer_2 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[1], self.n_features[1]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='L2')
        self.down_layer_3 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[2], self.n_features[2]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='L3')
        self.down_layer_4 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[3], self.n_features[3]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='L4')
        self.down_layer_C_1 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[0], self.n_features[0]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='C_L1')
        self.down_layer_C_2 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[1], self.n_features[1]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='C_L2')
        self.down_layer_C_3 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[2], self.n_features[2]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='C_L3')
        self.down_layer_C_4 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[3], self.n_features[3]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='C_L4')
        
        self.down_layer_CC_1 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[0], self.n_features[0]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_L1')
        self.down_layer_CC_2 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[1], self.n_features[1]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_L2')
        self.down_layer_CC_3 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[2], self.n_features[2]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_L3')
        self.down_layer_CC_4 = UNetBlock('DOWNSAMPLE',
                                      (self.n_features[3], self.n_features[3]),
                                      (3, 3), with_downsample_branch=True,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_L4')


        c1_crop_amount=spatial_size_incl_surrounding//16
        c2_crop_amount=spatial_size_incl_surrounding//32
        c3_crop_amount=spatial_size_incl_surrounding//64
        c4_crop_amount=spatial_size_incl_surrounding//128 # want to get to a sixteenth of the size, need to crop a 32th per side, divide by two again because we relate to spatial size incl surrounding
        self.c1_crop_layer = tf.keras.layers.Cropping3D(cropping=(c1_crop_amount, c1_crop_amount, c1_crop_amount))
        self.c2_crop_layer = tf.keras.layers.Cropping3D(cropping=(c2_crop_amount, c2_crop_amount, c2_crop_amount))
        self.c3_crop_layer = tf.keras.layers.Cropping3D(cropping=(c3_crop_amount, c3_crop_amount, c3_crop_amount))
        self.c4_crop_layer = tf.keras.layers.Cropping3D(cropping=(c4_crop_amount, c4_crop_amount, c4_crop_amount))

        self.cc1_crop_layer = tf.keras.layers.Cropping3D(cropping=(c1_crop_amount, c1_crop_amount, c1_crop_amount))
        self.cc2_crop_layer = tf.keras.layers.Cropping3D(cropping=(c2_crop_amount, c2_crop_amount, c2_crop_amount))
        self.cc3_crop_layer = tf.keras.layers.Cropping3D(cropping=(c3_crop_amount, c3_crop_amount, c3_crop_amount))
        self.cc4_crop_layer = tf.keras.layers.Cropping3D(cropping=(c4_crop_amount, c4_crop_amount, c4_crop_amount))

        self.concat_layer_cs1 = ElementwiseLayer('CONCAT', name='cropskip')
        self.concat_layer_cs2 = ElementwiseLayer('CONCAT', name='cropskip')
        self.concat_layer_cs3 = ElementwiseLayer('CONCAT', name='cropskip')
        self.concat_layer_cs4 = ElementwiseLayer('CONCAT', name='cropskip')
        
        self.concat_layer_ccs1 = ElementwiseLayer('CONCAT', name='cropskip_coarsest_third')
        self.concat_layer_ccs2 = ElementwiseLayer('CONCAT', name='cropskip_coarsest')
        self.concat_layer_ccs3 = ElementwiseLayer('CONCAT', name='cropskip_coarsest')
        self.concat_layer_ccs4 = ElementwiseLayer('CONCAT', name='cropskip_coarsest')

        self.bottom_layer_CC = UNetBlock('UPSAMPLE',
                                      (self.n_features[4], self.n_features[3]),
                                      (3, 3), with_downsample_branch=False,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='bottom')

        self.bottom_layer_C = UNetBlock('UPSAMPLE',
                                      (self.n_features[4], self.n_features[3]),
                                      (3, 3), with_downsample_branch=False,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='bottom')


        self.bottom_layer = UNetBlock('UPSAMPLE',
                                      (self.n_features[4], 2*self.n_features[3]),
                                      (3, 3), with_downsample_branch=False,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='bottom')
        self.up_layer_4 = UNetBlock('UPSAMPLE',
                                    (2*self.n_features[3], 2*self.n_features[2]),
                                    (3, 3), with_downsample_branch=False,
                                    w_initializer=self.initializers['w'],
                                    w_regularizer=self.regularizers['w'],
                                    acti_func=self.acti_func,
                                    name='R4')
        self.concat_layer_4 = ElementwiseLayer('CONCAT')
        self.up_layer_3 = UNetBlock('UPSAMPLE',
                                    (2*self.n_features[2], 2*self.n_features[1]),
                                    (3, 3), with_downsample_branch=False,
                                    w_initializer=self.initializers['w'],
                                    w_regularizer=self.regularizers['w'],
                                    acti_func=self.acti_func,
                                    name='R3')
        self.concat_layer_3 = ElementwiseLayer('CONCAT')
        self.up_layer_2 = UNetBlock('UPSAMPLE',
                                    (2*self.n_features[1], 2*self.n_features[0]),
                                    (3, 3), with_downsample_branch=False,
                                    w_initializer=self.initializers['w'],
                                    w_regularizer=self.regularizers['w'],
                                    acti_func=self.acti_func,
                                    name='R2')
        self.concat_layer_2 = ElementwiseLayer('CONCAT')
        self.end_convolutional_layer = UNetBlock('NONE',
                                                 (2*self.n_features[0], 2*self.n_features[0]),
                                                 (3, 3),
                                                 with_downsample_branch=False,
                                                 w_initializer=self.initializers['w'],
                                                 w_regularizer=self.regularizers['w'],
                                                 acti_func=self.acti_func,
                                                 name='R1_Conv')
        self.concat_layer_1 = ElementwiseLayer('CONCAT')
        self.classification_layer = UNetBlock('NONE',
                                              (self.num_classes,),
                                              (1,),
                                              with_downsample_branch=False,
                                              w_initializer=self.initializers['w'],
                                              w_regularizer=self.regularizers['w'],
                                              acti_func=None,
                                              name='R1_Classification')

        self.up_layer_C_4 = UNetBlock('UPSAMPLE',
                                    (self.n_features[3], self.n_features[2]),
                                    (3, 3), with_downsample_branch=False,
                                    w_initializer=self.initializers['w'],
                                    w_regularizer=self.regularizers['w'],
                                    acti_func=self.acti_func,
                                    name='C_R4')
        self.concat_layer_C_4 = ElementwiseLayer('CONCAT', name='coarse')
        self.up_layer_C_3 = UNetBlock('UPSAMPLE',
                                    (self.n_features[2], self.n_features[1]),
                                    (3, 3), with_downsample_branch=False,
                                    w_initializer=self.initializers['w'],
                                    w_regularizer=self.regularizers['w'],
                                    acti_func=self.acti_func,
                                    name='C_R3')
        self.concat_layer_C_3 = ElementwiseLayer('CONCAT', name='coarse')
        self.up_layer_C_2 = UNetBlock('UPSAMPLE',
                                    (self.n_features[1], self.n_features[0]),
                                    (3, 3), with_downsample_branch=False,
                                    w_initializer=self.initializers['w'],
                                    w_regularizer=self.regularizers['w'],
                                    acti_func=self.acti_func,
                                    name='C_R2')
        self.concat_layer_C_2 = ElementwiseLayer('CONCAT', name='coarse')
        self.end_convolutional_layer_C = UNetBlock('NONE',
                                                 (self.n_features[0], self.n_features[0]),
                                                 (3, 3),
                                                 with_downsample_branch=False,
                                                 w_initializer=self.initializers['w'],
                                                 w_regularizer=self.regularizers['w'],
                                                 acti_func=self.acti_func,
                                                 name='C_R1_Conv')
        self.concat_layer_C_1 = ElementwiseLayer('CONCAT',  name='coarse')

        self.up_layer_CC_4 = UNetBlock('UPSAMPLE',
                                      (self.n_features[3], self.n_features[2]),
                                      (3, 3), with_downsample_branch=False,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_R4')
        self.concat_layer_CC_4 = ElementwiseLayer('CONCAT', name='coarse')
        self.up_layer_CC_3 = UNetBlock('UPSAMPLE',
                                      (self.n_features[2], self.n_features[1]),
                                      (3, 3), with_downsample_branch=False,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_R3')
        self.concat_layer_CC_3 = ElementwiseLayer('CONCAT', name='coarse')
        self.up_layer_CC_2 = UNetBlock('UPSAMPLE',
                                      (self.n_features[1], self.n_features[0]),
                                      (3, 3), with_downsample_branch=False,
                                      w_initializer=self.initializers['w'],
                                      w_regularizer=self.regularizers['w'],
                                      acti_func=self.acti_func,
                                      name='CC_R2')
        self.concat_layer_CC_2 = ElementwiseLayer('CONCAT', name='coarse')
        self.end_Convolutional_layer_CC = UNetBlock('NONE',
                                                   (self.n_features[0], self.n_features[0]),
                                                   (3, 3),
                                                   with_downsample_branch=False,
                                                   w_initializer=self.initializers['w'],
                                                   w_regularizer=self.regularizers['w'],
                                                   acti_func=self.acti_func,
                                                   name='CC_R1_Conv')
        self.concat_layer_CC_1 = ElementwiseLayer('CONCAT', name='coarse')
        
        
        self.coarse_classification_layer = UNetBlock('NONE',
                                              (self.num_classes,),
                                              (1,),
                                              with_downsample_branch=False,
                                              w_initializer=self.initializers['w'],
                                              w_regularizer=self.regularizers['w'],
                                              acti_func=None,
                                              name='R1_C_Classification')

        self.coarsest_classification_layer = UNetBlock('NONE',
                                              (self.num_classes,),
                                              (1,),
                                              with_downsample_branch=False,
                                              w_initializer=self.initializers['w'],
                                              w_regularizer=self.regularizers['w'],
                                              acti_func=None,
                                              name='R1_CC_Classification')



        crop_per_side_highres=(spatial_size_incl_surrounding//4)+(spatial_size_incl_surrounding//8)
        crop_per_side_coarse=(spatial_size_incl_surrounding//4)

        self.crop_layer_highres = tf.keras.layers.Cropping3D(cropping=(crop_per_side_highres,crop_per_side_highres,crop_per_side_highres))
        self.crop_layer_coarse_res = tf.keras.layers.Cropping3D(cropping=(crop_per_side_coarse,crop_per_side_coarse,crop_per_side_coarse))
        self.resolution_reduce_layer_2 = tf.keras.layers.AvgPool3D(pool_size=(2,2,2), strides=(2,2,2), padding='same')
        self.resolution_reduce_layer_4 = tf.keras.layers.AvgPool3D(pool_size=(4, 4, 4), strides=(4, 4, 4), padding='same')
        self.resolution_reduce_layer_maxpool_2 = tf.keras.layers.MaxPool3D(pool_size=(2,2,2), strides=(2,2,2), padding='same')
        self.resolution_reduce_layer_maxpool_4 = tf.keras.layers.MaxPool3D(pool_size=(4,4,4), strides=(4,4,4), padding='same')
        self.concat_layer_coarse_and_fine = ElementwiseLayer('CONCAT', name='coarse_fine_concat')
        self.coarse_loss_layer = CoarseClassificationLoss(metric_name='coarse_dice_plus_xent_metric')
        self.coarsest_loss_layer = CoarseClassificationLoss(metric_name='very_coarse_dice_plus_xent_metric')


    def train_step(self, data):
        """ Train step that is executed in fit. Only apply loss to first output of model.
        See also: https://www.tensorflow.org/guide/keras/customizing_what_happens_in_fit?hl=en
        Args:
            data:

        Returns:

        """
        x, y = data
        y_center=self.crop_layer_highres(y)  # we reduce the size of the input within the model, do the same to the gt

        with tf.GradientTape() as tape:
            y_pred, y_coarse_pred, y_coarsest_pred = self(x, training=True)  # Forward pass

            # auxilliary loss
            y_coarse = self.crop_layer_coarse_res(y)
            y_coarse = self.resolution_reduce_layer_maxpool_2(y_coarse)
            y_coarsest = self.resolution_reduce_layer_maxpool_4(y)
            coarse_segmentation_loss = self.coarse_loss_layer(y_coarse, y_coarse_pred, training=True)

            coarsest_segmentation_loss = self.coarsest_loss_layer(y_coarsest, y_coarsest_pred[0], training=True)

            # Compute the primary loss value
            # (the loss function is configured in `compile()`)
            loss = self.compiled_loss(y_center, y_pred, regularization_losses=self.losses)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y, y_pred)
        # Return a dict mapping metric names to current value
        return {m.name: m.result() for m in self.metrics}


    def call(self, original_input, training=None):
        """

        :param thru_tensor: the input is modified in-place as it goes through the network
        :param training:
        :param unused_kwargs:
        :return:
        """
        # print('one voxel of the image: ', input.numpy()[0, 0, 0, 0, 0])

        image_input = original_input[0]

        hires_center = self.crop_layer_highres(image_input)
        lowres_cropped = self.crop_layer_coarse_res(image_input)
        lowres_with_surroundings = self.resolution_reduce_layer_2(lowres_cropped)
        very_lowres_with_surroundings = self.resolution_reduce_layer_4(image_input)
        primary_input=hires_center

        # image_size  should be divisible by 16 because of max-pooling 4 times, 2x2x2
        assert layer_util.check_spatial_dims(primary_input, lambda x: x % 16 == 0)
        thru_tensor = primary_input

        thru_tensor, conv_1 = self.down_layer_1(thru_tensor)
        thru_tensor, conv_2 = self.down_layer_2(thru_tensor)
        thru_tensor, conv_3 = self.down_layer_3(thru_tensor)
        thru_tensor, conv_4 = self.down_layer_4(thru_tensor)
        
        thru_tensor_C, conv_C_1 = self.down_layer_C_1(lowres_with_surroundings)
        thru_tensor_C, conv_C_2 = self.down_layer_C_2(thru_tensor_C)
        thru_tensor_C, conv_C_3 = self.down_layer_C_3(thru_tensor_C)
        thru_tensor_C, conv_C_4 = self.down_layer_C_4(thru_tensor_C)
        
        thru_tensor_CC, conv_CC_1 = self.down_layer_CC_1(very_lowres_with_surroundings)
        thru_tensor_CC, conv_CC_2 = self.down_layer_CC_2(thru_tensor_CC)
        thru_tensor_CC, conv_CC_3 = self.down_layer_CC_3(thru_tensor_CC)
        thru_tensor_CC, conv_CC_4 = self.down_layer_CC_4(thru_tensor_CC)

        crop_skip_concat_1= self.c1_crop_layer(conv_C_1)
        crop_skip_concat_2= self.c2_crop_layer(conv_C_2)
        crop_skip_concat_3= self.c3_crop_layer(conv_C_3)
        crop_skip_concat_4= self.c4_crop_layer(conv_C_4)
        
        ccrop_skip_concat_1= self.cc1_crop_layer(conv_CC_1)
        ccrop_skip_concat_2= self.cc2_crop_layer(conv_CC_2)
        ccrop_skip_concat_3= self.cc3_crop_layer(conv_CC_3)
        ccrop_skip_concat_4= self.cc4_crop_layer(conv_CC_4)

        thru_tensor_latent = self.concat_layer_cs4(thru_tensor, ccrop_skip_concat_4)
        thru_tensor_latent = self.concat_layer_cs4(thru_tensor_latent, crop_skip_concat_4)
        thru_tensor_latent, _ = self.bottom_layer(thru_tensor_latent)

        concat_4 = self.concat_layer_cs3(ccrop_skip_concat_3, crop_skip_concat_3)
        concat_4 = self.concat_layer_cs3(concat_4, conv_4)
        concat_4 = self.concat_layer_4(concat_4, thru_tensor_latent)
        thru_tensor, _ = self.up_layer_4(concat_4)

        concat_3 = self.concat_layer_3(ccrop_skip_concat_2, crop_skip_concat_2)
        concat_3 = self.concat_layer_3(concat_3, conv_3)
        concat_3 = self.concat_layer_3(concat_3, thru_tensor)
        thru_tensor, _ = self.up_layer_3(concat_3)

        concat_2 = self.concat_layer_2(ccrop_skip_concat_1, crop_skip_concat_1)
        concat_2 = self.concat_layer_2(concat_2, conv_2)
        concat_2 = self.concat_layer_2(concat_2, thru_tensor)
        thru_tensor, _ = self.up_layer_2(concat_2)

        concat_1 = self.concat_layer_1(conv_1, thru_tensor)
        thru_tensor, _ = self.end_convolutional_layer(concat_1)

        if training:
            # downsample :2 branch
            thru_tensor_C, _ = self.bottom_layer_C(thru_tensor_C)
            concat_C_4 = self.concat_layer_C_4(conv_C_4, thru_tensor_C)
            thru_tensor_C, _ = self.up_layer_C_4(concat_C_4)

            concat_C_3 = self.concat_layer_C_3(conv_C_3, thru_tensor_C)
            thru_tensor_C, _ = self.up_layer_C_3(concat_C_3)

            concat_C_2 = self.concat_layer_C_2(conv_C_2, thru_tensor_C)
            thru_tensor_C, _ = self.up_layer_C_2(concat_C_2)

            concat_C_1 = self.concat_layer_C_1(conv_C_1, thru_tensor_C)
            thru_tensor_C, _ = self.end_convolutional_layer_C(concat_C_1)

            coarse_segmentation, _ = self.coarse_classification_layer(thru_tensor_C)

            # downsample :4 branch
            thru_tensor_CC, _ = self.bottom_layer_CC(thru_tensor_CC)
            concat_CC_4 = self.concat_layer_CC_4(conv_CC_4, thru_tensor_CC)
            thru_tensor_CC, _ = self.up_layer_CC_4(concat_CC_4)

            concat_CC_3 = self.concat_layer_CC_3(conv_CC_3, thru_tensor_CC)
            thru_tensor_CC, _ = self.up_layer_CC_3(concat_CC_3)

            concat_CC_2 = self.concat_layer_CC_2(conv_CC_2, thru_tensor_CC)
            thru_tensor_CC, _ = self.up_layer_CC_2(concat_CC_2)

            concat_CC_1 = self.concat_layer_CC_1(conv_CC_1, thru_tensor_CC)
            thru_tensor_CC, _ = self.end_Convolutional_layer_CC(concat_CC_1)

            coarsest_segmentation = self.coarsest_classification_layer(thru_tensor_CC)

        thru_tensor, _ = self.classification_layer(thru_tensor)

        if training:
            return thru_tensor, coarse_segmentation, coarsest_segmentation
        else:
            return thru_tensor


