import sys
from typing import Dict, List

import numpy as np
import tensorflow as tf

from global_config import NIFTY_NET_PATH

sys.path.insert(0, NIFTY_NET_PATH)
from niftynet.utilities.util_common import ParserNamespace
from niftynet.engine.sampler_balanced_v2 import BalancedSampler
from niftynet.engine.sampler_grid_v2 import GridSampler
from niftynet.io.image_sets_partitioner import ImageSetsPartitioner
from niftynet.io.image_reader import ImageReader
from niftynet.layer.pad import PadLayer
from niftynet.engine.signal import INFER, VALID

from nonewnet.types import Action, DatasetToInfer


class GeneratorAdapter:

    def __init__(self, sampler, num_priors=0):
        self.sampler = sampler
        self.num_priors = num_priors

    def train_generator_with_prior(self):
        """
        An adapter converting NiftyNet's sampler to a generator with two inputs and label.
        :returns
            Tuple(image, prior1, prior2...., label)
        """

        while True:
            data_dict = self.sampler.pop_batch_op()
            image = tf.cast(data_dict['image'], tf.float32)[:, :, :, :, 0:1]
            inputs_list = [image]

            # add priors
            for prior_idx in np.arange(1, 1+self.num_priors):
                prior = tf.cast(data_dict['image'], tf.float32)[:, :, :, :, prior_idx:prior_idx+1]
                inputs_list.append(prior)

            label = tf.cast(data_dict['label'], tf.float32)
            yield tuple(inputs_list), label

    def inference_generator_with_prior(self):
        """
        An adapter converting NiftyNet's sampler to a generator with two inputs and label.
        :returns
            Tuple(image, image_location)
        """

        try:
            while True:
                data_dict = self.sampler.pop_batch_op()
                image = tf.cast(data_dict['image'], tf.float32)[:, :, :, :, 0:1]

                inputs_list = [image]

                # add priors
                for prior_idx in np.arange(1, 1 + self.num_priors):
                    prior = tf.cast(data_dict['image'], tf.float32)[:, :, :, :, prior_idx:prior_idx + 1]
                    inputs_list.append(prior)

                image_location = data_dict['image_location']
                yield tuple(inputs_list), image_location

        except tf.errors.OutOfRangeError:
            print('No more dataset iterations left.')


def get_image_readers_with_prior(action: Action, label_csv: str, ct_csv: str, dataset_split_file: str, win_size: int,
                                 prior_csv_list, dataset_to_infer: DatasetToInfer) -> List[ImageReader]:
    data_config = {'ct': ParserNamespace(
        csv_file=ct_csv,
        interp_order=1,
        pixdim=None,
        axcodes=None,
        spatial_window_size=(win_size, win_size, win_size),
        loader=None
    )}

    if not action is Action.infer: # we only need the label if we train or evalutate not for pure inference
        data_config['label'] =  ParserNamespace(
                csv_file=label_csv,
                interp_order=0,
                pixdim=None,
                axcodes=None,
                spatial_window_size=(win_size, win_size, win_size),
                loader=None
            )

    image_inputs = ['ct']

    if prior_csv_list:  # if prior_csv_list is empty or None, this will not be executed
        for idx, prior_file in enumerate(prior_csv_list):
            prior_key = 'prior_{}'.format(idx)
            data_config[prior_key] = ParserNamespace(
                csv_file=prior_file,
                interp_order=0,
                pixdim=None,
                axcodes=None,
                spatial_window_size=(win_size, win_size, win_size),
                loader=None
            )
            image_inputs.append(prior_key)

    # initialise input image readers
    if action is Action.train:
        reader_names = ['image', 'label', 'weight', 'sampler']
    elif action is Action.infer:
        reader_names = ['image']
    elif action is Action.evaluate:
        reader_names = ['image', 'label', 'inferred']

    task = ParserNamespace(image=tuple(image_inputs), label=('label',), sampler=('label',))
    data_partitioner = ImageSetsPartitioner()
    data_partitioner.initialise(data_config, data_split_file=dataset_split_file).get_file_list()
    if action is action.train:  # this will create a train and a validation reader
        file_lists = data_partitioner.get_file_lists_by(action='train')
    else:  # this will create just one reader, with the specified dataset
        file_lists = data_partitioner.get_file_lists_by(phase=dataset_to_infer.value, action='inference')
        file_lists.extend(
            file_lists)  # a weird fix, preventing even weirder errors during inference of more than one scan.
    readers = [ImageReader(reader_names).initialise(
        data_config, task, file_list) for file_list in file_lists]
    return readers


def add_volume_padding(readers, padding: int = 24):
    supported_input = set(['image', 'label', 'weight', 'sampler', 'inferred'])
    volume_padding_layer = [PadLayer(
        image_name=supported_input,
        border=(padding, padding, padding),
        mode='edge',  # choose any mode available for np.pad
        pad_to=(0,))
    ]
    # only add padding also to validation reader or inference reader
    readers[0].add_preprocessing_layers(
        volume_padding_layer)
    readers[1].add_preprocessing_layers(
        volume_padding_layer)
    return readers


def get_balanced_train_sampler(readers, window_size, batch_size):
    window_sizes = {'image': (window_size, window_size, window_size), 'label': (window_size, window_size, window_size),
                    'sampler': (window_size, window_size, window_size)}

    samplers = [[BalancedSampler(
        reader=reader,
        window_sizes=window_sizes,
        batch_size=batch_size,
        windows_per_image=20,
        queue_length=20) for reader in readers]]
    sampler = samplers[0][0]

    return sampler


def get_grid_inference_sampler(readers, window_size, infer_overlap):
    window_sizes = {'image': (window_size, window_size, window_size), 'label': (window_size, window_size, window_size),
                    'sampler': (window_size, window_size, window_size)}

    infer_overlaps = (infer_overlap, infer_overlap, infer_overlap)
    samplers = [[GridSampler(
        reader=reader,
        window_sizes=window_sizes,
        window_border=infer_overlaps,
        batch_size=1,
        queue_length=5) for reader in
        readers]]
    sampler = samplers[0][-1]

    return sampler
