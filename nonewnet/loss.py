import tensorflow as tf


def labels_to_one_hot(ground_truth, num_classes=1):
    """
    Converts ground truth labels to one-hot, sparse tensors.
    Used extensively in segmentation losses.

    :param ground_truth: ground truth categorical labels (rank `N`)
    :param num_classes: A scalar defining the depth of the one hot dimension
        (see `depth` of `tf.one_hot`)
    :return: one-hot sparse tf tensor
        (rank `N+1`; new axis appended at the end)
    """
    # read input/output shapes
    if isinstance(num_classes, tf.Tensor):
        num_classes_tf = tf.cast(num_classes, tf.int32)
    else:
        num_classes_tf = tf.constant(num_classes, tf.int32)
    input_shape = tf.shape(ground_truth)
    output_shape = tf.concat(
        [input_shape, tf.reshape(num_classes_tf, (1,))], 0)

    if num_classes == 1:
        # need a sparse representation?
        return tf.reshape(ground_truth, output_shape)

    # squeeze the spatial shape
    ground_truth = tf.reshape(ground_truth, (-1,))
    # shape of squeezed output
    dense_shape = tf.stack([tf.shape(ground_truth)[0], num_classes_tf], 0)

    # create a rank-2 sparse tensor
    ground_truth = tf.cast(ground_truth, tf.int64)

    ids = tf.range(tf.cast(dense_shape[0], tf.int64), dtype=tf.int64)
    ids = tf.stack([ids, ground_truth], axis=1)
    one_hot = tf.SparseTensor(
        indices=ids,
        values=tf.ones_like(ground_truth, dtype=tf.float32),
        dense_shape=tf.cast(dense_shape, tf.int64))

    # resume the spatial dims
    one_hot = tf.sparse.reshape(one_hot, output_shape)
    return one_hot


def reshape_pred_and_gt(prediction: tf.Tensor, ground_truth: tf.Tensor, num_classes: tf.Tensor):
    """

    :param prediction: tensor of shape (1,wx,wy,wz,num_classes)
    :param ground_truth: tensor of shape (1,wx,wy,wz,1)
    :param num_classes:
    :return:
        pred_b: tensor of shape (wx*wy*wz, num_classes)
        ground_truth_b: tensor of shape (wx*wy*wz,)
    """

    pred_b = tf.reshape(prediction, [-1, num_classes])

    # reshape pred, ground_truth, weight_map to the same
    # size: (n_voxels, num_classes)
    # if the ground_truth has only one channel, the shape
    # becomes: (n_voxels,)
    if not pred_b.shape.is_fully_defined():
        ref_shape = tf.stack(
            [tf.shape(pred_b)[0], tf.constant(-1)], 0)
    else:
        ref_shape = pred_b.shape.as_list()[:-1] + [-1]

    ground_truth_b = tf.reshape(ground_truth, ref_shape)
    if ground_truth_b.shape.as_list()[-1] == 1:
        ground_truth_b = tf.squeeze(ground_truth_b, axis=-1)

    return pred_b, ground_truth_b


class DicePlusXentLoss(tf.keras.losses.Loss):

    def call(self, ground_truth, prediction):
        num_classes = tf.shape(prediction)[-1]

        prediction, ground_truth = reshape_pred_and_gt(prediction=prediction, ground_truth=ground_truth,
                                                       num_classes=num_classes)

        prediction = tf.cast(prediction, tf.float32)

        # cross entropy
        ground_truth_int = ground_truth
        if len(ground_truth_int.shape) == len(prediction.shape):
            ground_truth_int = ground_truth_int[..., -1]

        ground_truth_int = tf.cast(ground_truth_int, tf.int32)

        entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            logits=prediction, labels=ground_truth_int)

        loss_xent = tf.reduce_mean(entropy)

        # DICE:
        one_hot = labels_to_one_hot(ground_truth, num_classes=num_classes)
        softmax_of_logits = tf.nn.softmax(prediction)

        dice_numerator = 2.0 * tf.sparse.reduce_sum(
            one_hot * softmax_of_logits, axis=[0])
        dice_denominator = \
            tf.reduce_sum(softmax_of_logits, axis=[0]) + \
            tf.sparse.reduce_sum(one_hot, axis=[0])

        epsilon = 0.00001
        loss_dice = -(dice_numerator + epsilon) / (dice_denominator + epsilon)

        return loss_dice + loss_xent


class XentLoss(tf.keras.losses.Loss):

    def call(self, ground_truth, prediction):
        num_classes = tf.shape(prediction)[-1]

        prediction, ground_truth = reshape_pred_and_gt(prediction=prediction, ground_truth=ground_truth,
                                                       num_classes=num_classes)

        prediction = tf.cast(prediction, tf.float32)

        # cross entropy
        ground_truth_int = ground_truth
        if len(ground_truth_int.shape) == len(prediction.shape):
            ground_truth_int = ground_truth_int[..., -1]

        ground_truth_int = tf.cast(ground_truth_int, tf.int32)

        entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            logits=prediction, labels=ground_truth_int)

        loss_xent = tf.reduce_mean(entropy)

        return loss_xent
