# Run using
# docker run --shm-size 8G -t --init --rm --gpus=0  \
# -v /home/"${USER}"/PhD:/home/"${USER}"/PhD  --name evatf2container tf2image:latest \
# python3 /home/"${USER}"/PhD/Repos/sneakynet/infer.py --infer_config.overlap=24
import logging
import os
import random
import sys

from absl import app
from absl import flags
from ml_collections.config_flags import config_flags
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.python.framework.ops import enable_eager_execution

import nonewnet.loss
import nonewnet.default_config
from nonewnet.data_io import add_volume_padding, GeneratorAdapter, \
    get_grid_inference_sampler, get_image_readers_with_prior
from global_config import NIFTY_NET_PATH, MODEL_DIR_NAME
from nonewnet.types import Action, DatasetToInfer

sys.path.insert(0, NIFTY_NET_PATH)
from niftynet.io.image_reader import ImageReader
from niftynet.engine.windows_aggregator_grid import GridSamplesAggregator
from niftynet.io.image_sets_partitioner import ImageSetsPartitioner
from niftynet.layer.pad import PadLayer
from niftynet.utilities.util_common import ParserNamespace
from niftynet.engine.sampler_grid_v2 import GridSampler

EAGER = True

if EAGER:
    enable_eager_execution(
        config=None,
        device_policy=None,
        execution_mode=None
    )

seed_value = 0
random.seed(seed_value)
np.random.seed(seed_value)
tf.random.set_seed(seed_value)

print('Start Inference: \n')
print('Tensor Flow version: ', tf.version.VERSION, '\n')
print('eager execution: ', tf.executing_eagerly()
      )
FLAGS = flags.FLAGS
# config_flags.DEFINE_config_file('infer_config', 'nonewnet/default_config.py')
config_flags.DEFINE_config_file('infer_config', 'nonewnet/mock_data_config.py')
flags.DEFINE_string("model_path",
                    '/path_to_file//Repos/sneakynet/debugmodel',
                    "This directory should contain a subdirectory called models which contains assets, variables, "
                    "keras_metadata.pb, saved_model.pb")
flags.DEFINE_string("predictions_save_dir", 'debug_images/prediction', "Path where the predictions will be saved.")
flags.DEFINE_integer("epoch", 10, "The epoch that should be loaded.")
flags.DEFINE_enum("dataset_to_infer", "valid", ["valid", "infer"], "Which dataset to infer.")


def infer(config, model_path, predictions_save_dir, dataset_to_infer):
    # Get dataset

    # Do we use a prior as input as well? Binary prior goes first
    prior_csv=[]
    if config.binary_csv:
        prior_csv.append(config.binary_csv)
    if len(config.prior_csv) > 0:
        prior_csv.extend(config.prior_csv)
    num_priors = len(prior_csv)
    num_input_channels = 1 + num_priors  # the usual input plus the priors

    readers = get_image_readers_with_prior(action=Action.infer, label_csv=config.label_csv, ct_csv=config.ct_csv,
                                dataset_split_file=config.dataset_split_file, win_size=config.win_size,
                                prior_csv_list=prior_csv, dataset_to_infer=dataset_to_infer)

    readers = add_volume_padding(readers, padding=config.padding)

    sampler = get_grid_inference_sampler(readers, window_size=config.win_size, infer_overlap=config.overlap)

    generator = GeneratorAdapter(sampler, num_priors)
    data_generator = generator.inference_generator_with_prior

    dset = tf.data.Dataset.from_generator(generator=data_generator,
                                          output_signature=(
                                              tf.TensorSpec(shape=(num_input_channels,  # the model input
                                                                   config.batch_size, config.win_size, config.win_size,
                                                                   config.win_size, 1),
                                                            dtype=tf.float32),
                                              tf.TensorSpec(shape=(1, 7),  # the image_location, needed to reassemble the whole scan from individual patches.
                                                            dtype=tf.float32)
                                          )
                                          )

    # Load model
    custom_objects = {"DicePlusXentLoss": nonewnet.loss.DicePlusXentLoss}
    keras_model = tf.keras.models.load_model(
        model_path, custom_objects=custom_objects, compile=True, options=None
    )

    # Create an output aggregator that will stitch the inferred patches together
    pred_path = predictions_save_dir
    output_aggregator = GridSamplesAggregator(image_reader=sampler.reader,
                                              window_border=(config.overlap, config.overlap, config.overlap),
                                              output_path=pred_path,
                                              interp_order=0,
                                              fill_constant=0.0)

    # output_aggregator_binary = GridSamplesAggregator(image_reader=sampler.reader,
    #                                           window_border=(config.overlap, config.overlap, config.overlap),
    #                                           output_path=pred_path,
    #                                           postfix='niftynet_out_binary',
    #                                           interp_order=0,
    #                                           fill_constant=0.0)

    with tf.keras.utils.custom_object_scope(custom_objects):
        # Inference for one patch (and hence also batch) at a time
        logging.info('Start inference:')
        for data_batch in dset:
            image = data_batch[0]
            image_location = data_batch[1].numpy()
            # Inference
            if config.output_binary_segmentation:
                if config.output_loc_prior:
                    net_output_tensor, y_binary_pred, y_prior = keras_model(image, training=False)
                else:
                    net_output_tensor, y_binary_pred = keras_model(image, training=False)
            else:
                net_output_tensor = keras_model(image, training=False)


            # Postprocessing
            net_output_tensor = tf.cast(tf.argmax(net_output_tensor, -1), tf.int32)
            net_output_tensor = tf.expand_dims(net_output_tensor, axis=-1)
            # binary_output_tensor = tf.cast(tf.argmax(binary_output_tensor, -1), tf.int32)
            # binary_output_tensor = tf.expand_dims(binary_output_tensor, axis=-1)
            logging.info(f'Inference of a patch with output_tensor shape {net_output_tensor.shape}')
            outputs = net_output_tensor.cpu().numpy()
            # outputs_binary = binary_output_tensor.cpu().numpy()
            # Add inferred patch to the aggregator
            output_aggregator.decode_batch({'window_seg': outputs}, image_location)
            # output_aggregator_binary.decode_batch({'window_seg': outputs_binary}, image_location)

    # output_aggregator._save_current_image()
    logging.info('Finished inference.')


def run_infer(_):
    if not os.path.exists(FLAGS.predictions_save_dir):
        os.makedirs(FLAGS.predictions_save_dir)

    # Need to remove the logging handlers, otherwise it will not write to file.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                        filename=os.path.join(FLAGS.predictions_save_dir, 'inference_log.log'),
                        filemode='w+', level=logging.INFO, datefmt='%m-%d %H:%M')

    with open(os.path.join(FLAGS.predictions_save_dir, 'inference_config.txt'), 'w+') as f:
        f.write(f'Inference using the following configs and args: \n'
                f'{FLAGS.infer_config} \n'
                f'--model_path {FLAGS.model_path} \n'
                f'--predictions_save_dir: {FLAGS.predictions_save_dir} \n')
    logging.info('Inference')

    dataset_to_infer = DatasetToInfer[FLAGS.dataset_to_infer]

    full_model_path = os.path.join(FLAGS.model_path, MODEL_DIR_NAME)
    if FLAGS.epoch > 0:
        full_model_path = os.path.join(full_model_path, str(FLAGS.epoch))

    infer(config=FLAGS.infer_config, model_path=full_model_path, predictions_save_dir=FLAGS.predictions_save_dir,
          dataset_to_infer=dataset_to_infer)
    for name in list(flags.FLAGS):
        delattr(flags.FLAGS, name)


if __name__ == '__main__':
    flags.register_validator('model_path', lambda path: os.path.isdir(path),
                             message='--model_path must be a path to an existing directory')
    app.run(run_infer)
