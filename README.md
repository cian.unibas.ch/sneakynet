# NoNewNet Keras Version for Disctinct Bone Segmentation

## Requires a TF2 compatible version of NiftyNet to be present!
https://gitlab.com/cian.unibas.ch/niftynettf2compat

## Preparation
Pull the TF2 Niftynet version.

In global_config.py set the NIFTY_NET_PATH variable to the path were the TF2 Niftynet is located on the disc.

In the Dockerfile modify the workdir variable, s.th. it points towards the location of this repo.

Also have a look at the training settings in default_config.py.

You will have to adjust the paths also in the mapping files contained in the sneakynet/data directory

## Training, Inference, Evaluation
For examples on how to run these, have a look at bash_scripts/run_baseline_32.sh