# Use tf-gpu image
FROM tensorflow/tensorflow:2.5.0-gpu

#nvidia changed keys
RUN apt-key del 7fa2af80
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/3bf863cc.pub

# Install required tools
RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get update && apt-get install -y sudo cmake curl

RUN pip install --upgrade pip

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

ENV MPLCONFIGDIR=/tmp
ENV TORCH_HOME=/tmp
ENV niftynet_config_home=/tmp
# for reproducibility
ENV PYTHONHASHSEED=0

WORKDIR /path/to/desired/workdir
